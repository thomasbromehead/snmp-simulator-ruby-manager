# SNMPSIM Rails manager
After much struggling trying to use variation modules with the wonderful SNMPSIM open-source tool from **Ilya Bergof** [here](https://snmplabs.thola.io/), I decided to create a small Rails app to let you write directly into the .snmprec file(s) that the simulator uses, as generated by the `snmpsim-record-commands`.\
This Rails app lets you load contents of your `snmprec` files into Redis which serves as the backbone for the front-end.
OID values can be changed in one-shot changes or through `ActiveJob` background jobs which update the front-end with `ActionCable` during their lifecycle.

It lets you load the `snmprec` files placed at the root with a rake task:
```ruby
  bundle exec rake register_recfiles
```
Support will be added for importing a single file at a later stage.

Record an SNMP device and direct the resulting `snmprec` file to live in this directory, or bring them in if they already exist.
Run the simulator with the following commands:
```bash
  cd ~/snmpsimulator/
  source venv/bin/activate

  CASE 1: app is there already
  cd snmp-simulator-ruby-manager

  CASE 2: no app yet
  Install git if needed
  git clone git@gitlab.com:thomasbromehead/snmp-simulator-ruby-manager.git
  cd snmp-simulator-ruby-manager

  # Check that you are on master
  git branch
  git checkout master (if not the case)

  # Check whether you are behind remote or not
  git rev-list --count --right-only HEAD...origin/master
  #If the number is greater than 0 integrate changes locally with
  git reset HEAD --hard (to avoid conflicts with local changes, if you want to keep them: `git status`, `git add file1 file2`, `git commit -m "message"`)
  git fetch origin master
  git rebase origin/master

  # If you need to generate a recording:
  snmpsim-record-commands --output-file=/path/to/rails/app/name.snmprec -c public|custom --agent-udpv4-endpoint=some_ip:some_port
  # OR possibly
  venv/bin/snmprec.py --agent-udpv4-endpoint=127.0.0.1:161 --output-file=./venv/snmpsim/data/test.snmprec  --community Nobius --start-oid=1.3.6.1.4.1.2021.13.16

  # Launch the simulator
  snmpsim-command-responder --agent-udpv4-endpoint=localhost|public_ip:port --data-dir=/path/to/rails/app
  # OR (not sure why binaries aren't always installed in the same way)
  ../venv/bin/snmpsimd.py --data-dir=.(or /path/to/rails/app) --agent-udpv4-endpoint=192.168.1.1:1161
```

1. Build the image: `docker-compose build app`
2. Install webpack: `docker-compose run app bin/rails webpacker:install`
3. Change volumes mapping to wherever your MIBS are located (defaults mapping is `/usr/share/snmp/mibs/:/usr/share/snmp/mibs/`)
And make sure you have the mibs necessary for the SNMP translate task (some mibs can be found in the mibs folder here)
4. Change the "HOST_SITE" parameter in config/environments/docker to the name of the site you are serving this on (nobius.ddns.net for now)
5. Start everything (not much): `docker-compose up -d` or debug it with: `docker-compose run --service-ports app`
6. Feed your snmprecs into Redis: `docker-compose exec app bundle exec rake translate (warning, can be long, until I move to JRuby interpreter)`
7. (optional, very first compose-up) Rails needs a DB: `docker-compose exec app bin/rails db:create`
8. (optional) Fork it and improve it

> **ADVICE:**
> In order to keep clean versions of your  `recfiles` you can either:
> 1. Keep clean versions on another branch by `git checkout -b clean-recs`, `git add *.snmprec`, `git commit -m "Clean SNMPREC files"` and go back to them at any time from master with `git checkout clean-recs -- raspberry.snmprec dahua.snmprec`
> 2. Keep clean files on level above this app and bring them back in each time

**Should you need to restart the app container: `docker-compose restart app`.
The Puma rails server is bound to `0.0.0.0` so you can navigate to it from any IP. It listens on port `3000`.**

### TODOS:
- Add more tests
- Add support for importing a single file
- Add support for Traps
- Add ability to cancel a single job

# Performance Benchmarks
TLDR: Need to move to JRuby to get real performance benefits, context-switching with the GVL yields no gains
__register_recfiles rake task__:
  - **Single file (3200 lines)**
    - 1 Single file / single threaded writing into Redis:
    0.201000   0.149027   0.350027 (0.802286)
    - 2. Single threaded writing into Postgres DB:
    19.118186   1.796802  20.914988 ( 43.150272)\

 - **Two files (3200 lines + 620 lines)**
    - 1. Single threaded:
      0.251451   0.092779   0.344230 (  0.515914)
    - 2. Two-threaded:
        0.264756   0.146814   0.411570 (  0.580296)\

 - **Ten files (5 x 3200 + 5 x 620)**
    - 1. Single threaded:
      1.737339   1.327994   3.065333 (  4.598112)
    - 2. Five-threaded:
      1.038643   0.542559   1.581202 (  2.248532)
    - 3. 10 Fibers
      0.823934   0.623299   1.447233 (  2.309137)

- **Twenty files (10 x 3200 + 10 x 620)**
    - 1. Single threaded:
      1.800650   1.102221   2.902871 (  4.628487)
    - 2. Two-threaded
      1.919258   1.235655   3.154913 (  4.760405)
    - 2. Five-threaded:
      1.923873   1.248110   3.171983 (  4.534341)
    - 3. 20 Fibers
      1.975319   0.994311   2.969630 (  4.721211)
    - 4. 2 Fibers
      2.111483   1.314670   3.426153 (  5.751279

__Controller Tasks__
 - 1. Retrieving all keys that match user search input then getting their values\
      __4 threads | 400 keys__:\
        0.021594   0.016051   0.037645 (  0.088756)
      __1 thread | 400 keys__:
        user     system      total        real
        0.031725   0.004390   0.036115 (  0.072410)
