require_relative 'redis_service'
require 'fileutils'

module TextService

  ErrorRemovingFromRedis = Class.new(StandardError)

  def self.encode_value(type, value, is_ip = false)
    encoded = case type
    when "4x"
      if is_ip
        # Process of encoding IP to hex as follows
        # Split the submitted value
        # Convert array entries to integers
        # Convert the array of 8 bit unsigned integers to a binary string
        # Convert the binary string to an array of 1 value equal to a 32 bit unsigned integer (that's what an ipv4 is)
        # Convert to a hex string
        ip_to_hex(value)
      else
        value.unpack("H*").first
      end
    when "64x"
      is_ip ? ip_to_hex(value) : value
    else
      value
    end
    encoded
  end

  def self.ip_to_hex(value)
    value.split(".").map(&:to_i).pack('CCCC').unpack('N').first.to_s(16)
  end

  def self.write_to_file(dataObject, path: "./")
    begin
      file_with_path = path + dataObject.filename
      lock_file_and_gsub(file_with_path, dataObject) do |new_content|
        File.open("temporary", "w+"){ |f| f.puts new_content }
        FileUtils.mv("temporary", file_with_path)
        # Broadcast changes to front-end
        ActionCable.server.broadcast 'variations', {
          type: "change_value",
          variation_oid: dataObject.oid,
          value: dataObject.new_set_value.split("|")[2]
       }
      end
      update_redis(dataObject)
    rescue EOFError
    rescue Exception => e
      Rails.logger.error e.message
      Rails.logger.error e.backtrace
    end
  end

  private

  def self.lock_file_and_gsub(file, dataObject)
    new_content = ""
    begin
      if File.exist?(file)
         File.open(file) do |f|
           f.flock(File::LOCK_EX)
           new_content = File.read(file).sub(dataObject.old_set_value, dataObject.new_set_value)
         end
        yield new_content
     else
        raise "File not Found"
      end
    ensure
      File.open(file).flock(File::LOCK_UN)
    end
  end


  def self.update_redis(dataObject)
    item_deleted = redis.zrem("#{dataObject.name}-sorted-set", dataObject.old_set_value)
    if item_deleted
      redis.multi do 
        redis.hset("#{dataObject.name}-offsets", "#{dataObject.start_index}:#{dataObject.oid}:#{dataObject.end_index}", dataObject.new_set_value)
        redis.zadd("#{dataObject.name}-sorted-set", dataObject.start_index, dataObject.new_set_value)
      end
    else 
      raise ErrorRemovingFromRedis.new("Failed removing #{dataObject.old_set_value} from redis")
    end
  end

  def self.redis
    ::Snmpapp.redis
  end
end
