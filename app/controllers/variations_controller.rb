class VariationsController < ApplicationController
  before_action :set_file_name, only: [:create, :build_variation_modal]
  prepend_before_action :set_name, only: [:create, :build_variation_modal]

  def create
    if params["snmp_variation"]["to"] == params["snmp_variation"]["from"]
      flash[:error] = "To and from are the same"
    end
    unless flash[:error]
      variation = Snmp::Variation.new(params_to_keywords(params["snmp_variation"]).merge({type: get_variation_type}))
      data = redis.hscan_each("#@name-offsets", match: "*:#{params["snmp_variation"]["oid"]}*")
      @variation = variation
      begin
        job = VariateJob.perform_later(Marshal.dump(variation), JSON.generate(data.to_a.first))
        flash[:success] = "Variation for #{params["snmp_variation"]["oid"]} being performed by job #{job.job_id}"
      rescue
        Rails.logger.error = $!.message
        Rails.logger.error = $!.backtrace
        flash[:error] = $!.message
      end
    end
    redirect_to rec_file_path(name: @name, cursor: $items_loaded, request_type: "variation")
  end


  def build_variation_modal
    snmp_variation = params["snmp_variation"]
    oid = snmp_variation.fetch("oid")
    from =  snmp_variation.fetch("from")
    @variation = Snmp::Variation.new(oid: oid, from: from, filename: @name)
    @is_open = true
    respond_to do |format|
      format.js {}
    end
  end

  private

  def get_variation_type
    if params["linear"]
      type = :linear
    elsif params["random"]
      type = :random
    else
      type = :linear
    end
    type
  end

  def params_to_keywords(params)
    {
      to: params["to"],
      from: params["from"],
      oid: params["oid"],
      filename: @filename,
      duration: params["duration"],
      interval: params["interval"]
    }
  end

  def set_file_name
    has_rec = @name.match(/\.snmprec/)
    @filename = has_rec ? @name : @name + ".snmprec"
  end

  def set_name
    @name = params.try(:[], "snmp_variation").try(:[], "filename")
    raise "You didn't pass a name in the snmp_variation['filename'] input field" unless @name
  end
end
