
class RecEntriesController < ApplicationController
  include ApplicationHelper
  require 'benchmark'
  before_action :find_rec_files, only: :home
  before_action :set_file_name, only: [:show_rec_file, :update, :find_entries]
  prepend_before_action :set_name, only: [:show_rec_file,:update, :find_entries]
  before_action :valid_request_type?, only: :show_rec_file

  IncorrectRequest = Class.new(StandardError)
  MAX_ENTRIES = 100
  MAX_RESULTS = 300

  $items_loaded = 0

  rescue_from Encoding::UndefinedConversionError do |exception|
    p $!.error_char.encoding  #"\xC0".encode('UTF-8', invalid: :replace, undef: :replace, replace: '?')
  end


  def home
  end

  def show_rec_file
    raise IncorrectRequest.new("Params should have the 'type' and 'cursor' keys") unless params["request_type"] && params["cursor"]
    if !valid_request_type?
      flash[:error] = "request_type param should be one of show_all, show_100, variation or build_variation or update or cursor should be present"
      redirect_back(fallback_location: root_path) and return
    end
    parameters, name = params, @name
    request_handler = Snmp::Request::Handler.new do
        @cursor = parameters["cursor"].to_i
        @request_type = parameters["request_type"]
        @filename = name
    end
    load_items_handler = request_handler.load_items_handler
    response = request_handler.build_response(load_items_handler)
    @cursor, @entries, @number_of_entries = load_items_handler.up_to, response.items, load_items_handler.number_of_entries
    raise IncorrectRequest.new("Cursor is greater than the number of entries") if @cursor > @number_of_entries
    @variation = Snmp::Variation.new(from: params["from"], oid: params["oid"], filename: params["name"]) if Snmp::Request::Handler::BUILD_VARIATION == request_handler.request_type
    if request_handler.request_type == "variation"
      jobs = VariateJob.class_variable_get(:@@active_jobs)
      unless jobs.empty?
        @variations = jobs.map do |job|
          Marshal.load(job.arguments.first)
        end
      end
    end
    respond_to do |format|
      format.html {}
      format.js {}
    end
  end

  def update
    submitted_value = params.try(:[], "value")
    redis_field_enum = params["offset"] || get_oid_offset(params["oid"])
    redis_field_array = redis_field_enum.to_a.first
    key, current_value = redis_field_array[0], redis_field_array[1]
    oid, tag = current_value.split('|')
    # Encode the submitted value
    # check whether the field is an ip
    ip = is_ip?
    encoded_value = TextService.encode_value(tag, submitted_value, ip)
    new_set_value = "#{oid}|#{tag}|#{encoded_value}"
    start_index = key.split(":")[0].to_i
    end_index = key.split(":")[2].to_i
    # Write to file, update hash of offset:oid => value
    # as well as sorted set with score: offset value: oid|tag|value
    TextService.write_to_file(Snmp::TransferData.new({
      filename: @filename,
      old_set_value: current_value,
      new_set_value: new_set_value,
      start_index: start_index,
      end_index: end_index,
      name: @name,
      oid: oid
    }))
    respond_to do |format|
      format.html {
       flash[:success] = "Changed #{params["oid"]} successfully to #{submitted_value}"
       redirect_to rec_file_path(name: @name, cursor: $items_loaded, request_type: "update")
     }
    end
  end

  # Works for finding all entries in the file as well as all entries under a specific OID
  def find_entries
    from_modal = request.headers["X-From-Modal"]
    oid_pattern = request.query_parameters["oid_pattern"].gsub(" ", "")
    # Check if the user is searching for a string or numeric OID representation
    is_string = !!oid_pattern.match(/^\D/)
    if is_string
      oids = get_oids_by_string(oid_pattern)
      @oid = oid_pattern
      results = oids.map { |oid| get_sorted_set_members("*#{oid}*") }.map {|enumerator| enumerator.to_a}.flatten(1)
    else
      @oid = oid_pattern
      results = get_sorted_set_members("*#{@oid}*")
    end
    @count ||= results.to_a.size
    # Check if same results from cache so we don't rebuild the modal in that case
    old_result_equals_new_result?(results) if from_modal
    redis_register_results(results.to_a)
    if too_many_results?
      @entries = []
      @warning = "There are #{@count} results under this branch, please refine your search (MAX allowed 300 in this view)"
    elsif @count != 0
      build_rec_entries(results, from_sorted_set_scan: true)
    else
       @entries = []
    end
    results_modal = render_to_string("shared/_results", layout: false)
    render json: {data: {is_open: true, modal_content: results_modal}, success: false} and return if @count == 0
    render json: {data: {is_open: true, same_results: true }, success: true} and return if @same_result
    render json: {data: {is_open: true, modal_content: results_modal}, success: true} if JSON.parse(results.to_json).all?
  end

  private

  def valid_request_type?
    %w(show_all show_100 update variation build_variation).include?(params["request_type"])
  end

  def is_ip?
    translation = redis.get(params["oid"])
    !!(translation.match /ip/i) || ["Address", "address"].any? {|pattern| !translation.scan(pattern).empty? }
  end

  def get_oid_offset(oid)
    redis.hscan_each("#@name-offsets", match: "*:#{oid}*")
  end

  def get_sorted_set_members(submitted_value)
    redis.get_sorted_set_members("#{@name}-sorted-set", submitted_value)
  end

  def old_result_equals_new_result?(results)
    old_value = redis.get("lookups:last_result")
    if old_value == results.to_a.to_s
      @same_result = true
      return true
    end
    false
  end

  def get_oids_by_string(oid_pattern)
    results = redis.find_keys("*#{oid_pattern}*")
    @count = results.length
    return if too_many_results?
    oids = results.map do |string_oid|
      redis.get(string_oid)
    end
    oids
  end

  def redis_register_results(value)
    redis.set("lookups:last_result", value)
  end

  def build_rec_entries(results, from_sorted_set_scan:nil)
    results = results.to_a.sort{|a,b| a.try(:[], 1) <=> b.try(:[], 1)}
    @entries = results.map do |entry|
      oid, tag, value = from_sorted_set_scan ? entry[0].split('|') : entry.split('|')
      translation = redis.get(oid)
      value = '' if value.nil?
      Snmp::Response::RecEntry.new(
        oid: oid,
        tag: tag,
        value: value,
        translation: translation,
        variation: Snmp::Variation.new(
          oid: oid,
          from: value,
          interval: 1,
          filename: @name
        )
      )
    end
  end

  def too_many_results?
    @count > MAX_RESULTS
  end

  def running_thread_count
    Thread.list.select {|thread| thread.status == "run"}.count
  end

  def get_values(keys)
    results = []
    keys.each do |k|
      results << redis.get(k)
    end
    results
  end

  def find_rec_files
    Dir.chdir(Rails.root)
    rec_files = Dir['*snmprec']
    return @rec_files = rec_files unless !rec_files.empty?
    @rec_files = rec_files.map {|f| f.gsub('.snmprec','')}
  end

  def set_file_name
    has_rec = @name.match(/\.snmprec/)
    @filename = has_rec ? @name : @name + ".snmprec"
  end

  def set_name
    @name = params.try(:[], "name")
    raise "You didn't pass the name params" unless @name
  end

  def set_entries
    @entries = RecEntry.where(name: @name).limit(20)
  end
end
