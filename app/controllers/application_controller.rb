class ApplicationController < ActionController::Base
  def redis
    ::Snmpapp.module_eval { $redis }
  end

  def filename
    @name + ".snmprec"
  end

end
