class VariationsChannel < ApplicationCable::Channel
  def subscribed
     stream_from "variations"
  end

  def unsubscribed
    stop_all_streams
  end
end
