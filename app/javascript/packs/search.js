  document.addEventListener('turbolinks:load', () => {
    const mainSearch = (event) => {
      user_input = event.target.value;
      // If text search allow search on fewer characters
      fetch(`/find_entries?oid_pattern=${user_input}&name=${filename}`)
      .then(res => res.json())
      .then(res => {
        const modal_container = document.getElementById("results-container");
        const no_results_container = document.getElementById("no-results");
        if(modal_container && res.success){
          no_results_container.innerText = "";
          search_input.value = "";
          modal_container.innerHTML = res.data.modal_content;
        } else {
          if(no_results_container){
            no_results_container.innerText = "NOTHING AT THIS OID"
          }
        }
      })
    }

    const modalSearch = (event, from_modal) => {
      user_input = event.target.value;
      const isNumeric = isNumericalSearch(user_input);
      let minChars;
      minChars = isNumeric ? 10 : 3
      if(user_input.length >= minChars && (!(event.keyCode == 190) || (event.keyCode == 13))){
        fetch(`/find_entries?oid_pattern=${user_input}&name=${filename}`, {
          headers: {
            'X-From-Modal': from_modal
          }
        })
        .then(res => res.json())
        .then(res => {
          if(res.data && res.data.same_results) return
          modal_container.innerHTML = res.data.modal_content;
        })
      }
    }

    const isNumericalSearch = (input) => {
      return !!(input.match(/^\.?\d+\.?/))
    }

    const debounce = (fn, event, delay, from_modal) => {
      let timer;
      clearTimeout(timer);
      timer = setTimeout(() => fn.call(this, event, from_modal), delay);
    }

    const success_banner = document.getElementById("flash-success");
    const dismiss_banner_button = document.getElementById('dismiss');
    if(dismiss_banner_button){
      dismiss_banner_button.addEventListener('click', () => {
        success_banner.style.display = 'none';
      })
    }
    const search_input = document.getElementById("js-search");
    modal_container = document.getElementById('results-container');
    const observer = new MutationObserver(() => {
      modal_container = document.getElementById("results-container");
      if(modal_container){
        let modal_search_input = document.getElementById("modal-js-search");
        modal_search_input.addEventListener('keydown', (event) => {
          const isNumeric = isNumericalSearch(event.target.value);
          let minChars;
          minChars = isNumeric ? 10 : 3
          // Don't trigger a search on a dot or backspace
          if(event.keyCode == 13 && event.target.value.length >= minChars){
            debounce(modalSearch, event, 400, false);
          }
        })
      }
    })
    observer.observe(modal_container, {subtree: true, childList: true});
    let filename = search_input.dataset.filename;
    if(search_input){
      search_input.addEventListener('keydown', (event) => {
        const isNumeric = isNumericalSearch(event.target.value);
        let minChars;
        minChars = isNumeric ? 10 : 3
        if(event.keyCode == 13 && event.target.value.length >= minChars ){
          debounce(mainSearch, event, 400, false);
        }
      })
    }
  })


