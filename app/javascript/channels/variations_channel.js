import consumer from "./consumer"

consumer.subscriptions.create("VariationsChannel", {
  connected() {
    // Called when the subscription is ready for use on the server
    console.log("CONNECTED", this.channel);
  },

  disconnected() {
    // Called when the subscription has been terminated by the server
  },

  received(data) {
    // Called when there's incoming data on the websocket for this channel
        var table = document.getElementById("variations-table");
        switch(data.type){
          case "no_jobs":
            table.innerHTML = data.table_head;
            break;
           case "remove_job":
            let row = document.getElementById(`variation-${data.variation_oid}` );
            if(row){
              row.remove();
            }
            break;
          case "change_value":
            var variation_value_cell = document.getElementById(`${data.variation_oid}-current-value`);
            var table_value_cell =  document.getElementById(`value-${data.variation_oid}`);
            if(variation_value_cell){
              variation_value_cell.innerText = data.value;
              if(table_value_cell){
                table_value_cell.outerHTML = `
              <input id="value-${data.variation_oid}" value="${data.value}" class="w-4/5 p-2 rounded" type="number" name="value">
                `;
              };
            }
            break;
        }
  }
});
