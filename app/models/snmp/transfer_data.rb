module Snmp
  class TransferData
    attr_reader :filename, :old_set_value, :new_set_value, :start_index, :end_index, :name, :redis_field, :oid
    def initialize(filename:, old_set_value:, new_set_value:, start_index:, end_index:, name:, oid:nil, redis_field:nil)
      @filename = filename
      @old_set_value = old_set_value
      @new_set_value = new_set_value
      @start_index = start_index
      @end_index = end_index
      @name = name
      @oid = oid
    end
  end
end
