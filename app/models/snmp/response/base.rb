module Snmp
  class Response::Base
    MAX_ENTRIES = 100
    attr_reader :from, :entry_numbers
    attr_accessor :items

    def initialize(load_handler)
      @to, @from, @entry_numbers = [load_handler.up_to, load_handler.from, load_handler.entry_numbers]
    end
  end
end
