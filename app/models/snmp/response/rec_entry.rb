module Snmp
  class Response::RecEntry
    attr_reader :oid, :tag, :offset, :translation, :id, :from, :filename
    attr_accessor :value,  :variation

    def initialize(oid:, tag:, value:, offset:nil, translation: "", id: 0, variation: nil, filename: "")
      @oid = oid
      @tag = tag
      @value = value
      @translation = translation
      @id = id
      @variation = variation
      @filename = filename
    end
  end
end
