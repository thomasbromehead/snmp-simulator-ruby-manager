module Snmp
  class Variation
    include ActiveModel::Model

    CURRENT_MORE_THAN_TO = "@current_value >= @to"
    CURRENT_LESS_THAN_TO = "@current_value <= @to"
    TIMER_EXPIRED = "start + @duration.to_i < Time.now.to_i"

    IncorrectVariationType = Class.new(StandardError)

    attr_reader :oid, :type, :duration, :to, :from, :filename, :interval, :current_value

    def initialize(
      oid:nil,
      duration:nil,
      type: :linear,
      to:nil,
      filename: nil,
      from:nil,
      interval:nil)
      @to = to.to_i
      @from = from.to_i
      @oid = oid
      @current_value = @from
      @type = type
      @filename = filename
      @interval = interval.to_i
      @duration = duration.to_i
    end

    def run(data)
      current_value, new_set_value, start_index, end_index  = prepare_values(data)
      set_pace
      runs = 0
      start = Time.now.to_i
      until eval(condition_is_met) do
        unless runs.zero?
          set_new_value
          @current_value = @to if eval(condition_is_met)
          new_set_value = (current_value.split('|')[0..1] << @current_value).join('|')
          transferData = Snmp::TransferData.new({
            filename: @filename,
            old_set_value: current_value,
            new_set_value: new_set_value,
            start_index: start_index,
            end_index: end_index,
            name: get_filename(no_suffix: true),
            oid: oid
          })
          TextService.write_to_file(transferData)
          current_value = new_set_value
        end
        runs += 1
        sleep(@interval)
      end
    end

    private

    def condition_is_met
      if linear?
       up? ? CURRENT_MORE_THAN_TO : CURRENT_LESS_THAN_TO
      elsif random?
        TIMER_EXPIRED
      end
    end

    def increment
      @current_value += @pace
    end

    def decrement
       @current_value -= @pace
    end

    def set_random_value
      range = @to > @from ? (@from..@to) : (@to..@from)
      @current_value = Random.new.rand(range)
    end

    def set_new_value
      if linear?
        up? ? increment : decrement
      elsif random?
        set_random_value
      else
        raise IncorrectVariationType.new("Request should be either linear or random")
      end
    end

    def linear?
      @type == :linear
    end

    def random?
      @type == :random
    end

    def distance
      (@to - @from).abs
    end

    def set_pace
      raise StandardError.new("To and From are the same") if @to == @from
      number_of_steps = (@duration/@interval).to_i
      @pace = up? ? (distance/number_of_steps).ceil : (distance/number_of_steps).floor
      Rails.logger.info "SET PACE TO: #{@pace}"
    end

    def get_direction
      @to > @from ? :up : :down
    end

    def up?
      get_direction == :up
    end

    def get_filename(options={})
      options[:no_suffix].present? ? @filename.sub(".snmprec", "") : @filename
    end

    def prepare_values(redis_field)
    key, current_value = redis_field[0], redis_field[1]
    oid, tag = current_value.split('|')
    # check whether the field is an ip
    # ip = is_ip?
    encoded_value = TextService.encode_value(tag, @to)
    new_set_value = "#{oid}|#{tag}|#{encoded_value}"
    start_index, end_index = key.split(":")[0].to_i,  key.split(":")[2].to_i
    [current_value, new_set_value, start_index, end_index]
    end
  end
end
