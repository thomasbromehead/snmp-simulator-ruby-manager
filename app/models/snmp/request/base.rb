module Snmp
  class Request::Base
    attr_reader :cursor, :up_to, :filename

    def initialize(cursor:, filename:)
      @cursor = cursor
      @filename = filename
    end

  end
end
