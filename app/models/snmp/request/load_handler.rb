  module Snmp
    class Request::LoadHandler

      attr_reader :how_many, :entry_numbers, :from, :number_of_entries, :up_to

      def initialize(request)
        @request = request
        @up_to = 0
        @entry_numbers = []
        @number_of_entries = total_entries
        set_from
        set_items_loaded
        build_entry_numbers
        set_how_many
      end

      private
      attr_reader :request

      def set_from
        if [Request::LoadAll, Request::Update].any? { |type| type === @request }
          @from = 0
        else
          @from = @request.cursor
        end
      end

      def cursor
        @request.cursor
      end

      def build_entry_numbers
        @up_to = $items_loaded
        # Max number of entries reached, entry numbers == [@cursor...max]
        if @up_to == total_entries
          @entry_numbers = (cursor..@up_to).to_a
        elsif @up_to <= Response::Base::MAX_ENTRIES
          @entry_numbers =  (0...100).to_a
        else
          @entry_numbers = Request::Update === @request ? (0..@up_to).to_a : (($items_loaded - 100)..$items_loaded).to_a
        end
      end

      def set_items_loaded
        return if Request::Update === @request
        dbsize = total_entries
          # Don't load anything else, it's just an update
        if (dbsize < Response::Base::MAX_ENTRIES || $items_loaded > dbsize)
          $items_loaded = dbsize
        else
          case request
          when Snmp::Request::LoadAll
            $items_loaded = dbsize
          when Snmp::Request::LoadSubset
            @from == 0 ? $items_loaded =  Response::Base::MAX_ENTRIES : $items_loaded += Response::Base::MAX_ENTRIES
          end
        end
        nil
        # $items_loaded = [Response::MAX_ENTRIES, $items_loaded].max
      end

      def set_how_many
        @how_many = case @request
                    when Request::LoadAll
                      total_entries
                    when Request::LoadSubset
                       Response::Base::MAX_ENTRIES
                    when Request::Update
                        $items_loaded
                    end
      end

      def total_entries
        ::Snmpapp.redis.zcard("#{@request.filename}-sorted-set")
      end
    end
  end
