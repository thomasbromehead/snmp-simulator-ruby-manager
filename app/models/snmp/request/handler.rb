module Snmp
  class Request::Handler
    UPDATE = "update"
    SHOW100 = "show_100"
    SHOWALL = "show_all"
    VARIATION = "variation"
    BUILD_VARIATION = "build_variation"

    module MyMockRedis
      def zrangebyscore(key:, min:, max:, limit:)
        results = super(key, min, max, limit: limit)
        data = yield results if block_given?
        data
      end
    end

    ::MockRedis.class_eval do
      prepend MyMockRedis
    end

    attr_reader :load_items_handler, :request, :items
    attr_accessor :cursor, :request_type, :filename

    def initialize(&block)
      instance_eval(&block)
      identify_request
      build_load_items_handler
    end

    def identify_request
      arguments = { cursor: @cursor, filename: @filename }
      @request = case @request_type
                 when UPDATE
                  Request::Update.new(arguments)
                 when VARIATION
                  Request::Update.new(arguments)
                 when BUILD_VARIATION
                   Request::Update.new(arguments)
                 when SHOW100
                   Request::LoadSubset.new(arguments)
                 when SHOWALL
                   Request::LoadAll.new(arguments)
                 end
    end

    def build_load_items_handler
      @load_items_handler = Request::LoadHandler.new(@request)
    end

    def build_response(load_handler)
      # Build response from request type and load_handler params
      @response = case @request
                  when Request::LoadAll
                    Response::LoadAll.new(load_handler)
                  when Request::LoadSubset
                    Response::LoadSubset.new(load_handler)
                  when Request::Update
                    Response::Update.new(load_handler)
                  end
      @response.items = get_redis_values
      @response
    end

    def get_redis_values
      results = ::Snmpapp.redis.zrangebyscore(key: "#{@filename}-sorted-set", min: "-inf", max: "+inf", limit: [load_items_handler.from, load_items_handler.how_many]) do |data|
            data.map do |r|
              oid, tag, value = r.split('|')
              value = '' if value.nil?
              translation =  ::Snmpapp.redis.get(oid)
              entry = Response::RecEntry.new(
                offset: @cursor,
                oid: oid,
                tag: tag,
                value: value,
                translation: translation,
                id: (load_items_handler.entry_numbers.shift + 1 rescue 0),
                filename: @filename
              )
              entry.variation = Snmp::Variation.new(oid: oid, from: value) if ["2", "65", "66", "67", "70"].include?(tag)
              entry
            end
      end
    end
  end
end
