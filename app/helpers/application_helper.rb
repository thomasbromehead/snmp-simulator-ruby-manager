module ApplicationHelper

  ASN1_TYPES = {
    "2"  => 'Integer32',
    "4"  => 'Octet String',
    "4x" => 'Hexified String',
    "5"  => 'null',
    "6"  => 'Object Identifier',
    "64" => 'Ip Address',
    "64x"=> 'ASCII String',
    "65" => 'Counter 32',
    "66" => 'Gauge 32',
    "67" => 'TimeTicks',
    "68" => 'Opaque',
    "68x"=> 'Don\'t know what that is yet...',
    "70" => 'Counter 64'
  }

  def is_numeric?(tag)
    %w(2 65 66 67 70).include?(tag)
  end

  def is_hex_or_ascii_string?(tag)
    result = ["Hexified String", "ASCII String"].include?(get_type(tag))
  end

  def decode_value(entry)
    begin
      binding.irb if entry.oid == "1.3.6.1.6.3.16.1.5.2.1.3.3.97.108.108.1.1"
      decoded_value = full_decode_value(entry.value, entry.translation)
    rescue StandardError => e
      return "#{entry.value}, translated as #{entry.translation} caused: #{e.message}"
    end
  end

  def get_type(tag)
    ASN1_TYPES.fetch(tag, "")
  end

  def full_decode_value(value, translation = nil)
    if translation
      if translation.match(/ip/i) || ["Address", "address"].any? {|pattern| !translation.try(:scan, pattern).try(:empty?)}
        # Process of decoding hex representation of the IP as follows:
        # Convert hex to Base 16 integer
        # Convert array to binary string
        # Convert string to array of 8-bit unsigned integers
        # Join everything together = YOUR IP!
        decoded = [value.to_i(16)].pack('N').unpack('CCCC').join('.')
      else
        decoded = pack_to_binary_hex(value)
      end
    else
      decoded = pack_to_binary_hex(value) rescue value
      # When the value is a hex-encoded ascii representing an ASN1 IP Address it previously failed to encode to ASCII
      # and rescued with the actual value: 10.1.1.0 => ASCII 10 is New Line Feed, 1 = Start of Heading, 0 = null
    end
    return decoded.force_encoding("UTF-8").valid_encoding?  ? decoded : value  
  end

  def pack_to_binary_hex(value)
    [value].pack('H*')
  end
end
