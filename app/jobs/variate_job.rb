
class VariateJob < ApplicationJob
  queue_as :default
  @@active_jobs = []

  CancelJobError = Class.new(StandardError)

  discard_on CancelJobError

  rescue_from(StandardError) do |ex|
    Rails.logger.error "This error was thrown inside job #{self.job_id}"
    Rails.logger.error ex.message
  end

  def perform(dumped_variation, data)
    Marshal.load(dumped_variation).run(JSON.parse(data))
  end

  before_enqueue do
    @@active_jobs << self
  end

  after_perform do
    Rails.logger.info "JOB #{self.job_id} finished"
    #  REMOVE THE LISTING FROM THE TABLE
    @@active_jobs.delete_if { |job| job.job_id == self.job_id }
    @variations = VariateJob.class_variable_get(:@@active_jobs)
    if @@active_jobs.empty?
      ActionCable.server.broadcast 'variations',
        {
          type: "no_jobs",
          table_head: VariationsController.render(partial: "jobs/variations_underway"),
        }
    else
      ActionCable.server.broadcast 'variations', {
        type: "remove_job",
        table_head: VariationsController.render(partial: "jobs/variations_underway"),
        variation_oid: Marshal.load(self.arguments.first).oid
      }
    end
  end
end
