require 'rails_helper'
require 'rake'


RSpec.describe 'Home Page', type: :request do

  let(:recfiles) do
    Dir.chdir(Rails.root)
    Dir['*snmprec']
  end

  before(){ get '/' }


  it "displays the list of available snmprec files" do
    recfiles.each do |file|
      expect(response.body).to match(file.gsub(".snmprec", ""))
    end
    expect(response.body).to match("SNMP Simulator Manager")
  end

  specify "each file name should be a link to /filename" do
    each_file { |file| expect(response.body).to match("<a href=\"/rec_entries/#{file}\">#{file}</a>") }
  end

  private

  def each_file
    recfiles.each do |file|
      yield clean_file_name(file)
    end
  end

  def clean_file_name(file)
    file.gsub(".snmprec", "")
  end

end
