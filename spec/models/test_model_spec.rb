require 'rails_helper'

describe Snmp::Toto, type: :model do
  it "the class shouts out" do
    allow(described_class).to receive(:shout_out).and_return("SHOUT OUT TO MY HOMIES")
    output = described_class.shout_out
    expect(output).to eq("SHOUT OUT TO MY HOMIES")
  end

  it "allows an instance to shout out" do
    expect_any_instance_of(described_class).to receive(:shout_out).and_call_original
    output = described_class.new.shout_out
    expect(output).to eq("INS")
  end
end

