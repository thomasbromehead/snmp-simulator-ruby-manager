require 'rails_helper'

# Remember to test for detection of an ip

module Snmp
  RSpec.describe TextService, type: :model do
    let(:ip) { RecEntry.new("4x", "192.168.1.20", true) }
    let(:regular_string){ RecEntry.new("4x", "(none)", false) }
    let(:encoded_ip){ described_class.encode_value(ip.type, ip.value, ip.is_ip) }
    let(:encoded_regular_string){ described_class.encode_value(regular_string.type, regular_string.value, regular_string.is_ip) }
    let(:timetick){ RecEntry.new("67", "38", false) }
    let(:encoded_timetick) { described_class.encode_value(timetick.type, timetick.value, timetick.is_ip) }
    let(:int32){ RecEntry.new("2", "393216", false) }
    let(:encoded_int32) { described_class.encode_value(int32.type, int32.value, int32.is_ip) }
    let(:counter64){ RecEntry.new("70", "993784", false)  }
    let(:encoded_counter64) { described_class.encode_value(counter64.type, counter64.value, counter64.is_ip) }
    let(:octet_string){ RecEntry.new("4", "swap", false) }
    let(:encoded_octet_string) { described_class.encode_value(octet_string.type, octet_string.value, octet_string.is_ip) }
    let(:counter32){ RecEntry.new("65", "19922", false) }
    let(:encoded_counter32) { described_class.encode_value(counter32.type, counter32.value, counter32.is_ip) }
    let(:object_identifier){ RecEntry.new("6", "1.3.6.1.6.3.10.3.1.1", false)  }
    let(:encoded_object_identifier) { described_class.encode_value(object_identifier.type, object_identifier.value, object_identifier.is_ip) }
    let(:null){ RecEntry.new("5", "26", false)  }
    let(:encoded_null) { described_class.encode_value(null.type, null.value, null.is_ip) }
    let(:opaque){ RecEntry.new("68", "2", false)  }
    let(:encoded_opaque) { described_class.encode_value(opaque.type, opaque.value, opaque.is_ip) }
    let(:gauge32){ RecEntry.new("66", "40407", false) }
    let(:encoded_gauge32) { described_class.encode_value(gauge32.type, gauge32.value, gauge32.is_ip) }

    let(:dataObject){ instance_double(TransferData) }
    let(:red){ instance_double(MockRedis) }

    it do
      expect(described_class).to respond_to(:encode_value)
    end

    it "properly encodes a hex-encoded ip string to utf8" do
      expect(encoded_ip).to eq("c0a80114")
      expect(encoded_ip.encoding).to be(Encoding::UTF_8)
    end

    it "properly encodes regular hex encoded strings" do
      expect(encoded_regular_string).to eq("286e6f6e6529")
    end

    it "doesn't encode timeticks, gauges, opaques, nulls, object identifiers, counter32/64, octet strings" do
      %w(counter32 counter64 octet_string object_identifier null gauge32 int32).each do |type|
        rspec_variable = eval(type)
        expect(eval("encoded_#{type}")).to eq(rspec_variable.value)
      end
    end

    it "writes an octet string into the snmprec properly" do
      allow(dataObject).to receive_messages({
        filename: "test.snmprec",
        old_set_value: "1.3.6.1.2.1.1.1.0|4|none",
        new_set_value: "1.3.6.1.2.1.1.1.0|4|this is a test",
        start_index: 0,
        end_index: 25,
        name: "test",
        oid: "1.3.6.1.2.1.1.1.0",
      })
      described_class.write_to_file(dataObject, $mr, path: Rails.root.join("spec/support"))
      expect($mr.hget("#{dataObject.name}-offsets", "#{dataObject.start_index}:#{dataObject.oid}:#{dataObject.end_index}")).to eq(dataObject.new_set_value)
      # expect($mr.zget("")).to eq()
      # expect(red).to have_received(:multi)
    end

    it "writes a hex ascii string into the snmprec properly" do
      allow(dataObject).to receive_messages({
        filename: "test.snmprec",
        old_set_value: "1.3.6.1.2.1.1.1.0|4|none",
        new_set_value: "1.3.6.1.2.1.1.1.0|4|this is a test",
        start_index: 0,
        end_index: 25,
        name: "test",
        oid: "1.3.6.1.2.1.1.1.0",
      })
      puts Dir.pwd
      described_class.write_to_file(dataObject, $mr, path: Rails.root.join("spec/support"))
        # Move file back to what it was initially
    end

    it "writes an object identifier into the snmprec properly" do
      allow(dataObject).to receive_messages({
        filename: "test.snmprec",
        old_set_value: "1.3.6.1.2.1.1.2.0|6|1.3.6.1.4.1.1004849.3.2.10",
        new_set_value: "1.3.6.1.2.1.1.2.0|6|1.3.6.1.4.1.1004849.3.4.19",
        start_index: 0,
        end_index: 25,
        name: "test",
        oid: "1.3.6.1.2.1.1.2.0",
      })
      puts Dir.pwd
      described_class.write_to_file(dataObject, $mr, path: Rails.root.join("spec/support"))
        # Move file back to what it was initially
    end

    it "writes a timetick into the snmprec properly" do
      allow(dataObject).to receive_messages({
        filename: "test.snmprec",
        old_set_value: "1.3.6.1.2.1.1.3.0|67|8608000",
        new_set_value: "1.3.6.1.2.1.1.3.0|67|9722"
        start_index: 0,
        end_index: 25,
        name: "test",
        oid: "1.3.6.1.2.1.1.3.0",
      })
      puts Dir.pwd
      described_class.write_to_file(dataObject, $mr, path: Rails.root.join("spec/support"))
      # Move file back to what it was initially
    end

    it "writes a gauge 32 into the snmprec properly" do
      allow(dataObject).to receive_messages({
        filename: "test.snmprec",
        old_set_value: "1.3.6.1.2.1.2.2.1.5.2|66|100000000",
        new_set_value: "1.3.6.1.2.1.2.2.1.5.2|66|20909090099",
        start_index: 0,
        end_index: 25,
        name: "test",
        oid: "1.3.6.1.2.1.2.2.1.5.2",
      })
      puts Dir.pwd
      described_class.write_to_file(dataObject, $mr, path: Rails.root.join("spec/support"))
      # Move file back to what it was initially
    end

    it "writes a counter 32 into the snmprec properly" do
      allow(dataObject).to receive_messages({
        filename: "test.snmprec",
        old_set_value: "1.3.6.1.2.1.2.2.1.16.2|65|1191036802",
        new_set_value: "1.3.6.1.2.1.2.2.1.16.2|65|1191029999",
        start_index: 0,
        end_index: 25,
        name: "test",
        oid: "1.3.6.1.2.1.2.2.1.16.2",
      })
      puts Dir.pwd
      described_class.write_to_file(dataObject, $mr, path: Rails.root.join("spec/support"))
      # Move file back to what it was initially
    end

    it "writes a counter 32 into the snmprec properly" do
      allow(dataObject).to receive_messages({
        filename: "test.snmprec",
        old_set_value: "1.3.6.1.2.1.1.1.0|4|none",
        new_set_value: "1.3.6.1.2.1.1.1.0|4|this is a test",
        start_index: 0,
        end_index: 25,
        name: "test",
        oid: "1.3.6.1.2.1.1.1.0",
      })
      puts Dir.pwd
      described_class.write_to_file(dataObject, $mr, path: Rails.root.join("spec/support"))
      # Move file back to what it was initially
    end

    it "writes a counter 64 into the snmprec properly" do
      allow(dataObject).to receive_messages({
        filename: "test.snmprec",
        old_set_value: "1.3.6.1.4.1.2021.4.26.0|70|834908",
        new_set_value: "1.3.6.1.4.1.2021.4.26.0|70|7523133",
        start_index: 0,
        end_index: 25,
        name: "test",
        oid: "1.3.6.1.4.1.2021.4.26.0",
      })
      puts Dir.pwd
      described_class.write_to_file(dataObject, $mr, path: Rails.root.join("spec/support"))
      # Move file back to what it was initially
    end

    it "writes an integer 32 into the snmprec properly" do
      allow(dataObject).to receive_messages({
        filename: "test.snmprec",
        old_set_value: "1.3.6.1.2.1.2.2.1.1.2|2|2",
        new_set_value: "1.3.6.1.2.1.2.2.1.1.2|2|234",
        start_index: 0,
        end_index: 25,
        name: "test",
        oid: "1.3.6.1.2.1.2.2.1.1.2",
      })
      puts Dir.pwd
      described_class.write_to_file(dataObject, $mr, path: Rails.root.join("spec/support"))
      # Move file back to what it was initially
    end


  end

  RecEntry = Struct.new(:type, :value, :is_ip)
end

