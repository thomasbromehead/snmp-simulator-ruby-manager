require 'rails_helper'
require 'fileutils'

RSpec.describe "lib/tasks/load_oids" do

  before(:context) do
    Rails.application.load_tasks
  end

  after(:each) do
    $mr.flushall
    Rake::Task["test:register_recfiles"].reenable
    Rake::Task["test:flush_keys"].reenable
    Dir.chdir(Rails.root.join("spec"))
  end

  context "the register_recfiles rake task" do

    it "should exit 1 if there are no snmprec files at the root of the app" do
      begin
        if File.exist?("./support/test.snmprec")
          FileUtils.mv("./support/test.snmprec", "./support/test")
        end
        expect { Rake::Task["test:register_recfiles"].invoke }.to raise_error(NoRecFile)
        # The Rake task puts you in spec/support
      ensure
        Dir.chdir(Rails.root.join("spec"))
        if File.exist?("support/test")
          FileUtils.mv("support/test", "support/test.snmprec")
        end
      end
    end

    it "should flush all keys in redis as a first step" do
      $mr.set("test", "test")
      expect($mr.dbsize).to eq(1)
      Rake::Task["test:flush_keys"].invoke
      expect($mr.dbsize).to eq(0)
    end

    context "creates two redis keys for each snmprec file" do
      before(:example) do
        Dir.chdir("support")
        Rake::Task["test:register_recfiles"].invoke
        Dir.chdir("..")
      end

      it "one key for each file" do
        expect($mr.dbsize).to eq(2)
        expect($mr.keys).to contain_exactly(*keys)
      end

      it "creates a filename-offsets key" do
        expect($mr.hlen "test-offsets").to eq(number_of_oids)
        expect($mr.hget("test-offsets","0:2.3.6.1.2.1.1.1.0:25")).to eq("2.3.6.1.2.1.1.1.0|4|none")
        expect($mr.hget("test-offsets", "274:2.3.6.1.2.1.2.2.1.5.2:309")).to eq("2.3.6.1.2.1.2.2.1.5.2|66|100000000")
      end

      it "creates a filename-sorted-set key" do
        expect($mr.zcard("test-sorted-set")).to eq(9)
        expect($mr.zrange("test-sorted-set", 0, +1000, with_scores: true)).to eq(sorted_set_values)
      end
    end

  describe"the translate rake task" do
    before(:example) do
      Dir.chdir("support")
      Rake::Task["test:translate"].invoke
    end

    after(:example) do
      Dir.chdir("..")
      Rake::Task["test:translate"].reenable
    end

    it "creates an oid key" do
      oids.each do |oid|
        expect($mr.exists(oid)).to eq(1)
      end
    end

    it "creates a key with the translation" do
      translations.values.each do |translation|
        expect($mr.exists(translation)).to eq(1)
      end
    end

    it "loads SNMP oids string representations into Redis" do
      oids[0..1].each do |oid|
        expect($mr.get(oid)).to eq(translations["#{oid}"])
      end
    end
  end
end
  private
  def number_of_oids
    number_as_string = `cat "support/test.snmprec" | wc -l`
    return number_as_string.to_i
  end
end
