require 'rails_helper'

RSpec.describe '/rec_entries', type: :request do

  let(:filename) {{ name: "dahua" }}
  let(:test) {{ name: "test" }}
  let(:first_load_params){{cursor: 0, request_type: "show_100" }}
  let(:second_load_params){{ cursor: 100, request_type: "show_100" }}
  let(:no_request_type) {{ cursor: 1 }}
  let(:no_cursor) {{ request_type: "show_all" }}
  let(:load_all_params) {{ request_type: "show_all", cursor: 0 }}
  let(:incorrect_request_type){{ cursor: 1, request_type: "show" }}

  context "AJAX Interactions" do
    context "when given incorrect params" do
      it "should redirect home if request_type is not recognized" do
        get "/rec_entries/dahua", params: incorrect_request_type.merge(filename)
        expect(response).to have_http_status(:redirect)
        follow_redirect!
        expect(response).to have_http_status(:success)
        expect(response.body).to match("request_type param should be one of show_all, show_100 or update")
      end

      it "should raise if request_type is missing" do
        expect { get "/rec_entries/dahua", params: no_request_type.merge(filename)}.to raise_error(RecEntriesController::IncorrectRequest)
      end


      it "should redirect home and flash if cursor is missing" do
        expect { get "/rec_entries/dahua", params: no_cursor.merge(filename)}.to raise_error(RecEntriesController::IncorrectRequest)
      end
    end

    context "when given valid request parameters" do
       before(:context) do
        Rails.application.load_tasks
        Rake::Task["test:register_recfiles"].invoke
      end

      after(:each) do
        Rake::Task["test:register_recfiles"].reenable
        Rake::Task["test:flush_keys"].reenable
      end

      after(:context) do
        $mr.flushall
      end

      it "should return a subset of 100 entries on first load" do
        get "/rec_entries/dahua", params: first_load_params.merge(test)
        expect(response).to have_http_status(:success)
        expect($items_loaded).to eq(100)
      end

      it "should set $items_loaded to 200 if called twice" do
        get "/rec_entries/dahua", params: first_load_params.merge(test)
        get "/rec_entries/dahua", params: second_load_params.merge(test)
        expect($items_loaded).to eq(200)
      end

      it "should load everything when request_type is load_all" do
        get "/rec_entries/dahua", params: load_all_params.merge(test)
        expect(response).to have_http_status(:success)
        expect($items_loaded).to eq(615)
      end

    end

  end

end
