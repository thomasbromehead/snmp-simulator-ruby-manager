require 'rails_helper'

RSpec.describe VariateJob, type: :job do
  include ActiveJob::TestHelper

  describe "#perform_later" do

    it "calls the #write_to_file method on TextService" do
      ActiveJob::Base.queue_adapter = :test
      text_service = class_double("TextService").as_stubbed_const
      expect(text_service).to receive(:do_this).once
      VariateJob.perform_later
    end

     it "enqueues writing into snmprec properly" do
       expect { VariateJob.perform_later }.to have_enqueued_job
     end
  end
end
