require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

$redis = nil

module Snmpapp
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.1
    config.autoload_paths << "lib/modules"
    config.action_view.automatically_disable_submit_tag = false

    # Configuration for the application, engines, and railties goes here.
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")
  end

  def self.redis_config
    Rails.application.config.redis.with_indifferent_access
  end


  Rails::Railtie::Configuration.new.after_initialize do
    if Rails.env.test?
      $redis = $mr
    else
      $redis = RedisService.redis
    end
    Rails.logger.info "Redis started: #{Snmpapp.redis_config}"
  end

  def self.redis
    $redis ||= RedisService.redis
  end
end
