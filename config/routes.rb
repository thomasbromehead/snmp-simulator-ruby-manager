Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: "rec_entries#home"
  get 'rec_entries/:name/(:cursor)', to: 'rec_entries#show_rec_file', as: :rec_file
  match 'rec_entries/update', to: 'rec_entries#update', as: :update_rec_entry, via: [:put, :patch]
  get 'benchmark/:name', to: 'rec_entries#benchmark'
  get 'find_entries', to: "rec_entries#find_entries"
  resources :variations, only: [:new, :create] do
    post :build_variation_modal, on: :collection, as: :build_variation_modal
    get "cancel_job/:id", to: "variations#destroy", on: :collection, as: :cancel_job
  end
end
