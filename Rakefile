# Add your own tasks in files placed in lib/tasks ending in .rake,
# for example lib/tasks/capistrano.rake, and they will automatically be available to Rake.

require_relative "config/application"

Rails.application.load_tasks

require_relative 'lib/modules/redis_service'
require 'benchmark'
require 'fiber'

=begin
The service reads the contents of the .snmprec files found at the root
and loads them into Redis.
We use one thread per file, up to 5 max to speed up the process.
Some of these files can be several thousands of lines long...!
=end

NoRecfile = Class.new(StandardError)

def filename(file)
  file = file.first if file.is_a?(Array)
  return file.gsub('.snmprec','')
end

def write_to_redis(file, callee:nil)
  filename = filename(file)
  file = file.first if file.is_a?(Array)
  f = File.open(file, 'r')
  Thread.current['name'] = filename
  case(callee)
  when :fiber
    puts "#{Fiber.current} handling file: #{file}"
  else
    puts "#{Thread.current['name']} thread handling file: #{file}"
  end
  begin
    start_index = f.pos
    value = f.gets.try(:chomp)
    end_index = f.pos
    next if value.nil? || value == ""
    begin
       oid = value.split('|')[0]
       RedisService.hset("#{filename}-offsets", {"#{start_index}:#{oid}:#{end_index}" => value})
       # sh "DOCKER_HOST=#{ENV['DOCKER_HOST']} docker-compose exec debian snmptranslate -m ALL -Ot #{oid}"
    rescue EOFError
    rescue RuntimeError
    end
    RedisService.zadd("#{filename}-sorted-set", start_index.to_i, value)
  end while(value)
end


desc 'translate'
task :translate => :register_recfiles do
  puts "Starting SNMPtranslates"
  name_only = Proc.new { |name| name.split('.')[0]}
  files = @rec_files.flatten.map{|f| name_only.call(f)}
  Benchmark.bm do |x|
    x.report("One Process per slice of 200 oids") do
      files.each do |file|
        RedisService.hkeys("#{file}-offsets") do |results|
          # Use the results to snmptranslate oids and enrich existing entries in dahua-sorted-set
          results.each_slice(200) do |slice|
            fork do
              slice.each do |oid_with_offset|
                oid = oid_with_offset.split(":")[1]
                translation = `snmptranslate -m ALL -Ot #{oid}`.chomp
                # Add translation to Redis
                RedisService.set(oid, translation)
                RedisService.set(translation, oid)
              end
            end
          end
        end
      end
    end
    Process.waitall
  end
  @rec_files = nil
end


desc 'register snmprec files'
task register_recfiles: :environment do
  Dir.chdir(Rails.root)
  @rec_files=Dir['*snmprec']
  RedisService.flush_all unless @rec_files.empty?
  if @rec_files.empty?
    raise NoRecFile.new("No snmprec files at the root of the app")
  elsif @rec_files.length == 1
    write_to_redis(@rec_files.first)
  else
    # Prepare threads if more than one file present
    threads = Array.new(2)
    @rec_files = [@rec_files[0...(@rec_files.length / 2)], @rec_files[((@rec_files.length)/2)..(@rec_files.length-1)]]
    # Flush everything before importing
    @rec_files.each_with_index do |files,i|
      ->(files) {
        t = Thread.new() do
          files.each { |f| write_to_redis(f)}
        end
        threads[i-1] = t
      }.call(files)
    end unless @rec_files.length == 20
  end

    # Remove existing entries
    # Benchmark.bm do |x|
    #   x.report do
    #     RecEntry.where(name: filename).destroy_all
    #     #RedisService.flush_all
    #     f = File.open(@rec_files.first, 'r')
    #     f.readlines.each do |line|
    #       oid, tag, value = line.split('|')
    #       value = '' if value.nil?
    #       RecEntry.create(oid: oid, tag: tag, value: value, name: filename)
    #     end
    #   end
    # end

    # Single threaded
    Benchmark.bm do |x|
      # -------------
      #### SINGLE-THREADED
      # -------------
      # x.report("single_threaded:\n") do
      #   @rec_files.each do |rec_file|
      #     write_to_redis(rec_file)
      #   end
      # end


      # -------------
      ####  MULTI-THREADED (max-threads = 5)
      # -------------
      if @rec_files.length > 1
        x.report("#{threads.count}_threaded: \n") {
          threads.each(&:join)
        }
      end

      # -------------
      #### FIBERS
      # -------------
      # @fibers = Array.new(2)
      # def create_fiber(files, index)
      #   f = Fiber.new do
      #     files.each {|rec_file| write_to_redis(rec_file, callee: :fiber)}
      #     Fiber.yield
      #   end
      #   puts $fibers
      #   @fibers[index] = f

      # end

      # x.report("Using Fibers: \n") do
      #   2.times do |i|
      #     i == 0 ? create_fiber(@rec_files[0...(@rec_files.length / 2)],i) : create_fiber(@rec_files[((@rec_files.length)/2)..(@rec_files.length-1)],i)
      #   end
      #   @fibers.each(&:resume)
      # end
    end
      #Rails.logger.info("Finished registering #{@rec_files.first}, found #{RecEntry.count} entries")
  end




